<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AuthTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }

    public function login(){
        $user = factory(\App\User::class)->create([
            'email' => 'test@gmail.com',
            'password' => bcrypt('password'),
        ]);

        $data = ['email' => 'test@gmail.com', 'password' => 'password'];

        $this->json('POST', 'api/login', $data)
            ->assertStatus(200);
        $user->delete();
    }
}
