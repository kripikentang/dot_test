<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RajaOngkirTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }

    public function testCity(){
        $response = $this->get('api/search/cities/1');
        $response->assertStatus(200);
    }

    public function testProvince(){
        $response = $this->get('api/search/provinces/1');
        $response->assertStatus(200);
    }
}
