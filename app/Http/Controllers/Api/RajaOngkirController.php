<?php

namespace App\Http\Controllers\Api;

use App\Model\City;
use App\Model\Province;
use App\Http\Controllers\Controller;
use GuzzleHttp;

class RajaOngkirController extends Controller
{
    private $api_key = '0df6d5bf733214af6c6644eb8717c92c';
    private $host = 'https://api.rajaongkir.com/starter/';

    public function sendRequest($endpoint, $method='GET'){
        try{
            $client = new GuzzleHttp\Client();
            $get = $client->request($method,$this->host.$endpoint,[
                'headers' => [
                    'key'        => $this->api_key,
                ]
            ]);
            return json_decode($get->getBody());
        }catch (\Exception $e){
            return json_decode(($e->getResponse()->getBody(true)));
        }
    }

    public function getProvince(){
        $res = $this->sendRequest('province');
        return json_encode($res);
    }

    public function getCity(){
        $res = $this->sendRequest('city');
        return json_encode($res);
    }


    public function getProvincesById($id){
        $res = $this->sendRequest('province?id='.$id);
        if ($res->rajaongkir->status->code == 200){
            return json_encode($res->rajaongkir->results);
        }else{
            return null;
        }
    }

    public function getCitiesById($id){
        $res = $this->sendRequest('city?id='.$id);
        if ($res->rajaongkir->status->code == 200){
            return json_encode($res->rajaongkir->results);
        }else{
            return null;
        }
    }

    public function save(){
        $now = date('Y-m-d H:i:s');
        $res = json_decode($this->getProvince());
        if ($res->rajaongkir->status->code == 200){
            foreach ($res->rajaongkir->results as $result){
                $province[] = [
                    'province_id'            => $result->province_id,
                    'province'          => $result->province,
                    'created_at'    => $now,
                    'updated_at'    => $now
                ];
            }
            Province::truncate();
            $insert = Province::insert($province);
            if (!$insert){
                return [
                    'status'    => 500,
                    'message'   => 'province not inserted'
                ];
            }
        }
        $res = json_decode($this->getCity());
        if ($res->rajaongkir->status->code == 200){
            foreach ($res->rajaongkir->results as $result){
                $city[] = [
                    'city_id'       => $result->city_id,
                    'province_id'   => $result->province_id,
                    'province'      => $result->province,
                    'type'          => $result->type,
                    'city_name'          => $result->city_name,
                    'postal_code'   => $result->postal_code,
                    'created_at'    => $now,
                    'updated_at'    => $now
                ];
            }
            City::truncate();
            $insert = City::insert($city);
            if (!$insert){
                return [
                    'status'    => 500,
                    'message'   => 'province not inserted'
                ];
            }
        }
        return [
            'status'    => 200,
            'message'   => 'fetch data from RajaOngkir success'
        ];
    }
}
