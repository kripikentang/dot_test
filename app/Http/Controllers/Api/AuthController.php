<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use GuzzleHttp;

class AuthController extends Controller
{
    public function login(Request $request){
        $client = new GuzzleHttp\Client();
        try{
            $login = $client->post(config('app.auth.url').'oauth/token', [
                'form_params' => [
                    'grant_type' => 'password',
                    'client_id' => config('app.auth.client_id'),
                    'client_secret' => config('app.auth.client_secret'),
                    'username' => $request->username,
                    'password' => $request->password
                ]
            ]);
            return $login->getBody();
        } catch (GuzzleHttp\Exception\RequestException $exception){
            if ($exception->getCode() == 400){
                return response()->json('Invalid Request. Please enter username or password.', $exception->getCode());
            } elseif ($exception->getCode() == 401){
                return response()->json('Your credentials are incorrect. Please try again.', $exception->getCode());
            }

            return response()->json('Something went wrong on server.', $exception->getCode());
        }
    }
}
