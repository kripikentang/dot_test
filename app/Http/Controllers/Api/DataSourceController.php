<?php

namespace App\Http\Controllers\Api;

use App\Model\City;
use App\Model\Province;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DataSourceController extends Controller
{
    public function city(Request $request){

        if (config('app.data_source')=='DB')
            $city = City::getCity($request->id); //get from database
        else
            $city = (new RajaOngkirController())->getCitiesById($request->id); //get from API rajaongkir
        return $this->response($city);
    }

    public function province(Request $request){
        if (config('app.data_source')=='DB')
            $province = Province::getProvince($request->id); // get from database
        else
            $province = (new RajaOngkirController())->getProvincesById($request->id); //get from API rajaongkir
        return $this->response($province);
    }
}
