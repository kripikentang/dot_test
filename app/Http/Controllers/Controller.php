<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function response($data){
        if (is_null($data))
            $return = [
                'status'    => 204,
                'response'  => $data
            ];
        else
            $return = [
                'status'    => 200,
                'response'  => $data
            ];
        return response()->json($return);
    }
}
