<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class StructureDataController extends Controller
{

    public function index(){
        return view('structure_data');
    }

    public function max2(Request $request){
        $array = explode(',',$request->array);
        $array = array_unique($array);
        rsort($array);
        $number = $array[1];
        $arr = $request->array;
        return view('structure_data',compact('number','arr'));
    }
}
