<?php

namespace App\Console\Commands;

use App\Http\Controllers\Api\RajaOngkirController;
use Illuminate\Console\Command;

class GetData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'getData';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetch all data province and city from RajaOngkir ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $res = (new RajaOngkirController())->save();
        $this->info($res['message']);
        return;
    }
}
