<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    public static function getCity($id)
    {
        return self::where('city_id',$id)->first();
    }
}
