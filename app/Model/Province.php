<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    public static function getProvince($id)
    {
        return self::where('province_id',$id)->first();
    }
}
