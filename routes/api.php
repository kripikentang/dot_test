<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//sprint 1
Route::get('get-city', 'Api\RajaOngkirController@getCity');
Route::get('get-province', 'Api\RajaOngkirController@getProvince');

Route::get('search/provinces/{id}', 'Api\RajaOngkirController@getProvincesById');
Route::get('search/cities/{id}', 'Api\RajaOngkirController@getCitiesById');

//sprint 2

Route::post('login', 'Api\AuthController@login');

Route::middleware('auth:api')->group(function (){
    Route::get('/user', function (Request $request) {
        return $request->user();
    });
    Route::get('ds/search/provinces/{id}', 'Api\DataSourceController@city');
    Route::get('ds/search/cities/{id}', 'Api\DataSourceController@province');
});
