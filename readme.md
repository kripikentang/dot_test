## DOT Backend Engineer Test

---

### How to use

- Clone the repository with __git clone__
- Copy __.env.example__ file to __.env__ and edit database credentials there
- Run __composer install__
- Run __php artisan key:generate__
- Run __php artisan migrate --seed__
- Run __php artisan passport:install__ then copy the `Client ID` and 
  `Client Secret` to `config/app`
- Run __php artisan serve__
- Credentials __dot@gmail.com__ / __password__.


### Structure Data Test

```url
[GET] /structure-data 
```

### Fetching Data From RajaOngkir

```ssh
$ php artisan getData
```

### Documentation

REST API for search province and city by id
```url
[GET] api/search/provinces/{province_id}
[GET] api/search/cities/{city_id}
```

### Login
```url
[POST] api/login
```
with parameter

 Params | Desc 
 ------|------
 username | email user
 password| password user

with data source from DB or RajaOngkir just set at `config/app`.
Set `DB` for database `RajaOngkir` for RajaOngkir.
```url
[GET] api/ds/search/provinces/{prvince_id}
[GET] api/ds/search/cities/{city_id}
```
with headers
```ssh
'Authorization' => 'Bearer '.{access_token from login}
```

### Unit Testing
```ssh
$ ./vendor/bin/phpunit
```

### Knowledge & Experience

For this answer you can read at `Knowledge&Experience.md`

---

### License

@kripikentang.

---