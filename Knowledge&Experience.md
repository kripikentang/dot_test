###Knowledge & Experience 

1. Apa tantangan terbesar yang pernah anda temui saat membuat web application dan
bagaimana anda menyelesaikannya dari sisi engineering nya?
   - Tantangan terbesar saya adalah mencoba framework baru dan mengimplementasikannya,
  menyelesaikannya dengan cari referensi dan tutorial yang bagus untuk mempelajari framework
  tersebut dari awal. 
2. Bagaimana penerapan clean code pada project anda?
   - Penerapan clean code yang saya gunakan itu membuat code sesuai kebutuhan mengurangi tag comment
agar lebih mudah dipahami dan dimengerti oleh developer lain dan sebisa mungkin tidak membuat
code yang berulang.
3. Untuk efisiensi pengerjaan project dalam tim, jelaskan bagaimana gitflow yang biasa
   anda lakukan.
   - Untuk gitflow biasanya dalam beberapa project saya membuat lebih dari satu branch selain master
    seperti development agar tidak terjadi sesuatu yang tidak diinginkan misalnya merge code yang
    tidak teliti hingga menyebabkan error dan lain-lain.
4. Apa yang anda ketahui dengan design pattern? Jika pernah menggunakan, jelaskan
   design pattern apa saja yang biasanya digunakan untuk menyelesaikan masalah
   software engineering di web application.
   - Saya tidak mengetahui tentang design pattern.
5. Apa anda bersedia ditempatkan onsite di Malang? Jika memang harus remote,
   bagaimana pengaturan waktu & availability dalam komunikasi dan kolaborasi
   pengerjaan project?
   - Saya bersedia ditempatkan onsite di Malang. Jika harus remote, untuk pengaturan
   waktu saya sesuaikan dengan jam kerja seperti biasanya dan untuk availability dalam komunikasi
   sya bersedia kapan saja jika dibutuhkan  
