<form action="{{url('max2')}}" method="post">
    {{csrf_field()}}
    <label>Search second largest number from array</label><br>
    <label for="test">Add number (separate with comma):</label>
    <input type="text" name="array" @if(isset($arr))value="{{$arr}}"@endif>
    <button type="submit">search</button>
</form>
@if(isset($number))
    The second largest from array: {{$number}}
@endif