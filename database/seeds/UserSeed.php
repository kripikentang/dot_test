<?php

use Illuminate\Database\Seeder;

class UserSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [

            ['id' => 1, 'name' => 'KripiKentang', 'email' => 'dot@gmail.com', 'password' => '$2y$10$Rw3AZN/Zqen4x4GaIcy8tupBMsSdzyT4BePdXAHxvD9VNUpS6G0Pe', 'remember_token' => '',],

        ];

        foreach ($items as $item) {
            \App\User::create($item);
        }
    }
}
